package jm.edu.uwimona.ourvle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Class to handle the retrieving of information from ourvle
 * @author Javon
 *
 */
public class InformationHandler
{
	private String uname,passwd;
	private String name;
	private static View view;
	static HttpClient client = new DefaultHttpClient();
	static ArrayList<String> courseUrl = new ArrayList<String>();
	String data="";
	File contextFile;


	public InformationHandler(String username,String password,View v)
	{
		uname = username;
		passwd = password;
		view = v;

	}

	public static ArrayList<String> getCourseUrl()
	{
		return courseUrl;
	}

	public String getData()
	{
		return data;
	}

	public void setData(String info)
	{
		data = info;
	}

	public String getCourses(String url) 
	{
		AsyncCaller caller = new AsyncCaller();
		caller.execute(url,"post");
		String result = null;

		try {
			result = caller.get();
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}

		return result;
	}

	public String getFile(String url, String name)
	{
		AsyncCaller caller = new AsyncCaller();
		caller.execute(url,"post",name);
		String result = null;
		try 
		{
			result = caller.get();
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}
		return result;

	}

	public ArrayList<String> getCourses() 
	{
		
		AsyncCaller caller = new AsyncCaller();
		caller.execute("http://ourvle.mona.uwi.edu/login/index.php","post");

		try {
			caller.get();
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}

		setName();
		//Log.d("Info",getData()+"here");

		String url = getData();

		ArrayList<String> links = null;
		if(url!= null)
		{
			links = findCourseLinks(url);
		}

		return links;
	}



	public static void addUrl(String url)
	{
		url=url.substring(url.indexOf("\"")+1);
		courseUrl.add(url);
	}


	public static ArrayList<String> findCourseLinks(String url) 
	{
		ArrayList<String> links = new ArrayList<String>();
		String courseParseInfo = "<a class=\"\" href=\"http://ourvle.mona.uwi.edu/course/view.php?id=";

		String expr = Pattern.quote(courseParseInfo)+"[0-9]+";

		Pattern pattern = Pattern.compile(expr);

		Matcher matcher = pattern.matcher(url);

		boolean found = false;
		while (matcher.find()) 
		{ 
			int closingTag = url.indexOf("</a>", matcher.end());

			links.add(android.text.Html.fromHtml(url.substring(matcher.end()+2,closingTag)).toString());

			addUrl(matcher.group().substring(matcher.group().indexOf("href")));

			found = true;
		}
		if(!found)
		{
			callToast(view,"Incorrect Login Information");
			
		}
		return links;
	}

	String info ="";

	public class AsyncCaller extends AsyncTask<String,Void,String>
	{
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();

			//this method will be running on UI thread

		}

		protected String doInBackground(String... params) 
		{	    	

			HttpPost post = new HttpPost(params[0]);
			try 
			{
				if(params[1]!="get")
				{
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

					nameValuePairs.add(new BasicNameValuePair("username",uname));
					nameValuePairs.add(new BasicNameValuePair("password", passwd));

					post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					HttpResponse response = client.execute(post);

					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

					String line = "";

					while ((line =reader.readLine()) != null) 
					{
						info+=line;
					}

					int status = response.getStatusLine().getStatusCode();
					Log.d("status Code", Integer.toString(status));



					setData(info);
				}
				else
				{
					File folder = new File(contextFile+"/Ourvle");
					if(!folder.exists())
						folder.mkdir();
					Log.e("params", params[0]+"gpd"+params[2]);
					File x = new File(folder,params[2]+".pdf");
					x.createNewFile();
					client.execute(new HttpGet(params[0])).getEntity().writeTo(new FileOutputStream(x));

				}

			} catch (IOException e) 
			{
				e.printStackTrace();
			}
			return info;

		}



		@Override
		protected void onPostExecute(String result)
		{
			super.onPostExecute(result);

		}
	}


	private static void callToast(View v,String msg)
	{
		Context context = v.getContext();
		//calcview.setText(calcview.getText().toString()+"(");
		final Toast notfunctional = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		notfunctional.show();
	}

	public void loadFile(String x, String name, File location) 
	{
		//TO-DO use download manager instead of async task to download
		String pdf = findLink(x);
		//Log.e("print",pdf);
		contextFile = location;
		
		AsyncCaller caller = new AsyncCaller();
		caller.execute(pdf,"get",name);

		try 
		{
			caller.get();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}
	}

	public static String findLink(String x) 
	{
		String textToFind = "<param name=\"src\" value=\"";
		String pdf ="";
		String expr = Pattern.quote(textToFind);

		Pattern pattern = Pattern.compile(expr);

		Matcher matcher = pattern.matcher(x);

		if(matcher.find())
		{
			String closing = "\"";
			pdf+=(x.substring(matcher.end(),x.indexOf(closing, matcher.end())));
		}

		Log.e("what's returned", pdf+"data");
		if(pdf.isEmpty())
		{
			Log.e("plaxe", "i got here");
			String textToFind2 = "Click <a href=\"";
			String expr2 = Pattern.quote(textToFind2);
			
			Pattern pattern2 = Pattern.compile(expr2);
			
			Matcher matcher2 = pattern2.matcher(x);
			
			if(matcher2.find())
			{
				String closing = "\"";
				pdf+=(x.substring(matcher2.end(),x.indexOf(closing, matcher2.end())));
			}
			
		}
		Log.e("what's returned", pdf+"data");
		return pdf;
	}

	/**
	 * @return the name
	 */
	public String getDisplayName() 
	{
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	private void setName() 
	{
		String site = getData();

		String textToFind = "title=\"View profile\">";

		String expr = Pattern.quote(textToFind);

		Pattern pattern = Pattern.compile(expr);

		Matcher matcher = pattern.matcher(site);

		if(matcher.find())
		{
			int complete = site.indexOf("</a>", matcher.end());

			this.name = site.substring(matcher.end(),complete);
		}

	}

}