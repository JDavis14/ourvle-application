package jm.edu.uwimona.ourvle;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

public class CourseOverviewActivity extends Activity {

	ArrayList<String> mycourses;
	TableLayout table;
	ListView courseList;
	CourseHandler obj;
	String displayName;

	public void setup()
	{
		table = (TableLayout) findViewById(R.id.table1);
		courseList = (ListView) findViewById(R.id.courseList);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_overview);

		displayName = getUsername();
		setTitle(displayName);
		
		setup();
		
		ArrayList<String> courses = getCourses();

		loadCourses(courses);
	}

	private String getUsername() 
	{
		Intent parent = getIntent();

		return parent.getStringExtra("name of user");
	}

	/**
	 * method to retrieve courses and put it in the form of an arraylist
	 * @return the ArrayList of courses
	 */
	private ArrayList<String> getCourses() 
	{
		Intent parent = getIntent();
		ArrayList<String> courses = parent.getStringArrayListExtra("courses");

		return courses;
	}

	public void loadCourses(ArrayList<String> courses)
	{
		mycourses = courses;

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(CourseOverviewActivity.this,R.layout.custom_textview,mycourses);

		courseList.setAdapter(adapter);
		courseList.setOnItemClickListener(new CourseListener());
	}

	private class CourseListener implements OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int position,long id) 
		{
			String courseName = mycourses.get(position);
			
			String courseCode = courseName.substring(0,8); //huge assumption for now
			
			obj = new CourseHandler(InformationHandler.getCourseUrl().get(position),view);
			Intent intent = new Intent(getApplicationContext(),CourseActivity.class);

			ArrayList<String> fileNames = obj.getInformation();
			ArrayList<String> urls = obj.getFileURLS();

			intent.putStringArrayListExtra("file names", fileNames);
			intent.putStringArrayListExtra("urls", urls);
			intent.putExtra("display name", displayName);
			intent.putExtra("course code", courseCode);
			
			startActivity(intent);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.course_overview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@SuppressWarnings("unused")
	private void callToast(View v,String msg)
	{
		Context context = v.getContext();
		//calcview.setText(calcview.getText().toString()+"(");
		final Toast notfunctional = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		notfunctional.show();
	}


}
