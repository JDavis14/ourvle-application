package jm.edu.uwimona.ourvle;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity class for login page
 * @author Javon
 *
 */
public class LoginActivity extends Activity 
{
	private EditText username,password;
	protected static String user_name,pass_word; 
	SharedPreferences preferences;
	private CheckBox remember;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		preferences = getPreferences(MODE_PRIVATE);
		
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		remember = (CheckBox) findViewById(R.id.remember_box);
		
		username.setText(preferences.getString("UserName",""));
		
		if(!username.getText().toString().isEmpty())
			remember.setChecked(true);
		
		remember.setOnCheckedChangeListener(new CheckListener());
	}
	
	/**
	 * method to go into course over view
	 * @param v - View
	 */
	public void callCourseViewActivity(View v)
	{
		//gets the username and the password from the EditTexts
		user_name = username.getText().toString();
		pass_word = password.getText().toString();

		//creates a new InformationHandler object, initializing with the username and the password given 
		//passes in the current view so that the handler can display messages properly if an error occurs
		InformationHandler obj = new InformationHandler(user_name,pass_word,v);
		
		ArrayList<String> courses = obj.getCourses();

		if(!courses.isEmpty())
		{
			String name = obj.getDisplayName();
			
			Intent intent= new Intent(this,CourseOverviewActivity.class);
			intent.putStringArrayListExtra("courses", courses);
			intent.putExtra("name of user",name);
			
			startActivity(intent);
			finish();
		}
	}
	
	
	private class CheckListener implements OnCheckedChangeListener
	{

		@Override
		public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) 
		{
			if(isChecked)
			{
				user_name = username.getText().toString();
				pass_word = password.getText().toString();
				if(!user_name.isEmpty())
				{
					SharedPreferences.Editor editor = preferences.edit(); //instanciates the editor
					editor.putString("UserName", user_name);
					editor.commit();
				}
			}
			else
			{
				SharedPreferences.Editor editor = preferences.edit(); //instanciates the editor
				editor.putString("UserName", "");
				editor.commit();
			}
		}
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("unused")
	private void callToast(View v,String msg)
	{
		Context context = v.getContext();
		final Toast notfunctional = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		notfunctional.show();
	}

}
