package jm.edu.uwimona.ourvle;

import java.io.File;
import java.util.ArrayList;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CourseActivity extends Activity 
{
	ArrayList<String> mycourses,myFiles;
	TextView heading;
	ListView fileList;
	ArrayList<String> urlList;
	Context context=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course);

		context = this;
		
		fileList = (ListView) findViewById(R.id.fileList);
		heading = (TextView) findViewById(R.id.headingText);
		
		heading.setText(getCCode());
		setTitle(getDisplayName());
		
		ArrayList<String> fileNames = getFiles();
		
		loadFiles(fileNames);
	}
	
	public String getCCode()
	{
		Intent parent = getIntent();
		
		return parent.getStringExtra("course code");
	}
	
	public String getDisplayName()
	{
		Intent parent = getIntent();
		
		return parent.getStringExtra("display name");
	}

	private void loadFiles(ArrayList<String> fileNames) 
	{
		myFiles = fileNames;

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(CourseActivity.this,R.layout.custom_textview,myFiles);

		fileList.setAdapter(adapter);
		fileList.setOnItemClickListener(new FileListener());
		
	}

	private class FileListener implements OnItemClickListener
	{

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int position,long id) 
		{
			String url = urlList.get(position);
			
			//String folder = "/storage/extSdCard/OURVLE/";
			
			File location = Environment.getExternalStorageDirectory();
			
			//Context context = getApplicationContext();
			
			String name = myFiles.get(position);
			
			Log.e("url", location.toString()+"data");

			new Download(url,view,name,location);
			
			File file = new File(location,"/OURVLE/"+name+".pdf");
			
			if (file.exists()) 
			{
			    Uri path = Uri.fromFile(file);
			    Intent intent = new Intent(Intent.ACTION_VIEW);
			    intent.setDataAndType(path, "application/pdf");
			    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			    try 
			    {
			        startActivity(intent);
			    } 
			    catch (ActivityNotFoundException e) 
			    {
			        callToast(view, "No Application Available to View PDF");
			         
			    }
			}	
			
		}
		
	}
	
	
	private ArrayList<String> getFiles() 
	{
		Intent parent = getIntent();
		ArrayList<String> files = parent.getStringArrayListExtra("file names");
		urlList = parent.getStringArrayListExtra("urls");
		
		return files;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.course, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private static void callToast(View v,String msg)
	{
		Context context = v.getContext();
		//calcview.setText(calcview.getText().toString()+"(");
		final Toast notfunctional = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		notfunctional.show();
	}


}
