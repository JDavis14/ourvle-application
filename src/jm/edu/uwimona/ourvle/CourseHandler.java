package jm.edu.uwimona.ourvle;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class CourseHandler 
{

	String url,info,data;
	private View view;
	ArrayList<String> fileURLS = new ArrayList<String>();


	public String getData() {
		return data;
	}

	/**
	 * @return the fileURLS
	 */
	public ArrayList<String> getFileURLS() {
		return fileURLS;
	}

	/**
	 * @param fileURLS the fileURLS to set
	 */
	public void setFileURLS(ArrayList<String> fileURLS) {
		this.fileURLS = fileURLS;
	}

	public void setData(String data2) {
		this.data = data2;
	}

	public CourseHandler(String urlToLoad,View v)
	{
		url = urlToLoad;
		view = v;
	}

	public ArrayList<String> getInformation()
	{
		InformationHandler obj = new InformationHandler(LoginActivity.user_name,LoginActivity.pass_word,view);

		String x = obj.getCourses(url);

		setData(x);
		ArrayList<String> PDFNames = getPDFS();
		return PDFNames;

	}

	private ArrayList<String> getPDFS() 
	{
		String page = getData();

		ArrayList<String> PDFnames = new ArrayList<String>();

		String textToFind1 = "<a class=\"\" onclick=\"";
		String textToFind2 = "\" href=\"http://ourvle.mona.uwi.edu/mod/resource/view.php?id=";

		String expr0 = Pattern.quote(textToFind1);
		String expr2 = Pattern.quote(textToFind2);

		String expr1 = expr0+"(.*?)"+expr2;
		//String urli = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

		String linkexpr = expr1+"[0-9]+";//+expr2+urli+Pattern.quote("\"")+" class=\"[a-z]+ [a-z]+\" alt=\" \" role=\"presentation\" /><span class=\"instancename\">";// alt=\"[a-zA-Z]+\"";//+expr3;//+"[a-zA-z]+"+expr4;

		String expr = linkexpr+"(.*?)<span class=\"instancename\">";

		Pattern pattern = Pattern.compile(expr);

		Pattern patternlink = Pattern.compile("http://ourvle.mona.uwi.edu/mod/resource/view.php"+Pattern.quote("?")+"id=[0-9]+");


		Matcher matcher = pattern.matcher(page);



		boolean found = false;
		while (matcher.find()) 
		{ 
			Matcher matcherlink = patternlink.matcher(matcher.group());
			if(matcherlink.find())
			{
				String link = matcherlink.group();
				Log.e("linksss", link);
				fileURLS.add(link);
			}
			int closingTag;

			if(page.indexOf("<span", matcher.end())<page.indexOf("</span", matcher.end()))
			{
				closingTag = page.indexOf("<span", matcher.end());
			}
			else
			{
				closingTag = page.indexOf("</span", matcher.end());
			}
			PDFnames.add(android.text.Html.fromHtml(page.substring(matcher.end(),closingTag)).toString());
		}

		found = true;
		if(!found)
		{
			callToast(view,"didnt find links");
		}
		return PDFnames;
	}

	private void callToast(View v,String msg)
	{
		Context context = v.getContext();
		final Toast notfunctional = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		notfunctional.show();
	}


}
